//Steps to do on Terminal

MacBook-Pro-di-Christian:~ christianvarriale$ sudo su
Password:
sh-3.2# pip install virtualenv

//after this, it will be a download:

sh-3.2# exit
exit
MacBook-Pro-di-Christian:~ christianvarriale$ cd Environments   -> (the folder can change) 
MacBook-Pro-di-Christian:Environments christianvarriale$ virtualenv --python=/usr/bin/python2.7 python27
....
MacBook-Pro-di-Christian:Environments christianvarriale$ source python27/bin/activate
(python27) MacBook-Pro-di-Christian:Environments christianvarriale$ python --version
Python 2.7.16
(python27) MacBook-Pro-di-Christian:Environments christianvarriale$ deactivate

//For version python3

MacBook-Pro-di-Christian:Environments christianvarriale$ virtualenv --python=/usr/bin/python3 python36

MacBook-Pro-di-Christian:Environments christianvarriale$ source python36/usr/local/bin/activate
(python36) MacBook-Pro-di-Christian:Environments christianvarriale$ python --version
Python 3.7.3

(python36) MacBook-Pro-di-Christian:Environments christianvarriale$ 
(python36) MacBook-Pro-di-Christian:Environments christianvarriale$ deactivate
MacBook-Pro-di-Christian:Environments christianvarriale$ source python27/bin/activate
(python27) MacBook-Pro-di-Christian:Environments christianvarriale$ pip install -U coremltools

//Finally:
(python27) MacBook-Pro-di-Christian:Environments christianvarriale$ deactivate
MacBook-Pro-di-Christian:Environments christianvarriale$ 


//Links:
https://pip.pypa.io/en/stable/installing/
https://stackoverflow.com/questions/31133050/virtualenv-command-not-found
https://stackoverflow.com/questions/48682118/python3-6-m-venv-env-fails
