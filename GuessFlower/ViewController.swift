//
//  ViewController.swift
//  WhatFlower
//
//  Created by Christian Varriale on 16/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import CoreML
import Vision
import Alamofire
import SwiftyJSON
import SDWebImage

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - Properties
    let imagePicker = UIImagePickerController()
    
    //Endpoint per fare le richieste
    let wikipediaURl = "https://en.wikipedia.org/w/api.php"
    
    //MARK: - IBoutlet
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelDescr: UILabel!
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
//        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //        let userPickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        
        if let userPickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            
//            imageView.image = userPickedImage
            
            guard let convertedImage = CIImage(image: userPickedImage) else { fatalError("Error nella conversione") }
            
            detect(image: convertedImage)
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }

    //MARK: - Function
    func detect(image: CIImage){
        
        //carico il modello importato da Inception3
        guard let model = try? VNCoreMLModel(for: FlowerClassifier().model) else { fatalError("Loading CoreML Model Failed.") }
        
        //creo una richiesta chiedendo al model di classificare i dati che gli passo
        let request = VNCoreMLRequest(model: model) { (request, error) in
            guard let results = request.results as? [VNClassificationObservation] else {
                fatalError("Model Failed to Process image")
            }

            //Nell'identifier si trova la stringa che mi serve, che passerò ad Alamo
            if let firstResult = results.first?.identifier.capitalized {
                
                self.navigationItem.title = firstResult
                self.navigationController?.navigationBar.barTintColor = UIColor.green
                self.requestInfo(flowerName: firstResult)
                
            }else{
                self.navigationItem.title = "Error!"
                self.navigationController?.navigationBar.barTintColor = UIColor.red
            }
                
        }
        
        //utilizzo un handler per effettuare la richiesta di classificazione dell'immagine
        let handler = VNImageRequestHandler(ciImage: image)
        
        do {
            
            try handler.perform([request])
            
        } catch {
            print(error)
        }
    }
    
    //MARK: - Alamofire Request
    func requestInfo(flowerName: String){
        
        //Questi Parametri sono stati presi dall'URL e verranno poi passati ad Alamofire
        //"https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=barberton%20daisy&indexpageids&redirects=1"
        
        //Da mettere nel JSON
        /*
         {"batchcomplete":"","query":{"normalized":[{"from":"barberton daisy","to":"Barberton daisy"}],"redirects":[{"from":"Barberton daisy","to":"Gerbera jamesonii"}],"pageids":["1276123"],"pages":{"1276123":{"pageid":1276123,"ns":0,"title":"Gerbera jamesonii","extract":"Gerbera jamesonii is a species of flowering plant in the genus Gerbera. It is indigenous to South Eastern Africa and commonly known as the Barberton daisy, the Transvaal daisy, and as Barbertonse madeliefie in Afrikaans."}}}}
         */
        
        let parameters : [String:String] = [
        "format" : "json",
        "action" : "query",
        "prop" : "extracts|pageimages",
        "exintro" : "",
        "explaintext" : "",
        "titles" : flowerName,
        "indexpageids" : "",
        "redirects" : "1",
        "pithumbsize" : "500"
        ]
        
        //Applico la richiesta che verrà gestita in un JSON
        Alamofire.request(wikipediaURl, method: .get, parameters: parameters).responseJSON { (response) in
            if response.result.isSuccess{
                print("Connessione con Wikipedia Avvenuta")
                
                let flowerJSON: JSON = JSON(response.result.value!)
                self.descriptionFlower(json: flowerJSON)
                
                print(response)
            }else{
                print("Error \(String(describing: response.result.error))")
            }
        }
    }
    
    //MARK: - Parsing JSON
    func descriptionFlower(json: JSON){
        
        let pageId = json["query"]["pageids"][0].stringValue
        
        let descFlower = json["query"]["pages"][pageId]["extract"].stringValue
        
        let flowerImg = json["query"]["pages"][pageId]["thumbnail"]["source"].stringValue
        
        print(flowerImg)
        
        updateDescrFlower(descrFlower: descFlower, flowerImg: flowerImg)
    }
    
    //MARK: - Update Outlet Storyboard
    
    func updateDescrFlower(descrFlower: String, flowerImg: String){
        imageView.sd_setImage(with: URL(string: flowerImg))
        labelDescr.text = descrFlower
        labelDescr.numberOfLines = 0
    }
    
    //MARK: - IBAction
    @IBAction func cameraTapped(_ sender: UIBarButtonItem) {
        present(imagePicker, animated: true, completion: nil)
    }
    
}

